from __future__ import unicode_literals

from django.apps import AppConfig
from material.frontend.apps import ModuleMixin

class PostsConfig(ModuleMixin, AppConfig):
    name = 'posts'
    icon = '<i class="mdi-communication-quick-contacts-dialer"></i>'