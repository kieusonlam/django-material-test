from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from django.views.generic import TemplateView

from .models import Post
# Create your views here.

class TestView(TemplateView):
	models = Post
	template_name = "posts/test.html"

def post_create(request):
	return HttpResponse("Create")

def post_detail(request, id=None):
	instance = get_object_or_404(Post, id=1)
	context = {
		"instance": instance,
	}
	return render(request, "posts/post_detail.html", context)
	
def post_list(request):
	queryset = Post.objects.all().order_by("-id")
	context = {
		"object_list": queryset,
	}
	return render(request, "posts/index.html", context)

def post_update(request):
	return HttpResponse("Update")

def post_delete(request):
	return HttpResponse("Delete")