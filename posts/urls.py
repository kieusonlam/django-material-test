from django.views import generic
from django.conf.urls import include, url
from django.contrib import admin

from views import TestView
from .views import (
	post_list,
	post_detail,
	post_create,
	post_update,
	post_delete,
	)

urlpatterns = [
    url(r'^$', "posts.views.post_list", name="index"),
    url(r'^detail/(?P<id>\d+)$', post_detail, name="detail"),
    # url(r'^create/$', post_create, name="create"),

    url(r'^test/$', TestView.as_view(), name="test"),
]